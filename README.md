## about

*gowttr* is an XMPP bot to query [wttr.in](https://wttr.in) for
the current weather by sending the location as a message.

It can also show links to the source code repo and the license. Send `!help`
to *gowttr* to see the available commands.

## requirements

* [go](https://golang.org/)

## installation

If you have *[GOPATH](https://github.com/golang/go/wiki/SettingGOPATH)* set just run this commands:

```
$ go get salsa.debian.org/mdosch/gowttr
$ go install salsa.debian.org/mdosch/gowttr
```

You will find the binary in `$GOPATH/bin` or, if set, `$GOBIN`.

## configuration

The configuration is expected at `$HOME/.config/gowttr/config.json` with this format:

```
{
"Address":	"example.com:5222",
"BotJid":	"weatherbot@example.com",
"Password":	"ChangeThis!",
"Debug":    false,
"PLAIN":    false
}
```
