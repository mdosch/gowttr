module salsa.debian.org/mdosch/gowttr

go 1.23.0

toolchain go1.23.5

require github.com/xmppo/go-xmpp v0.2.10

require (
	golang.org/x/crypto v0.36.0 // indirect
	golang.org/x/net v0.37.0 // indirect
)
